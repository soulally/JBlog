package com.esccase.blog;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Hooks;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class LangTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void reactorDebug(){
        Mono<Integer> source = new Random().nextBoolean()
                ? Flux.range(1,10).elementAt(5)
                : Flux.just(1,2,3,4).elementAt(5);

        source //
            .subscribeOn(Schedulers.parallel())
            .block();
    }

    @Test
    void reactorHookDebug(){
        Hooks.onOperatorDebug();

        Mono<Integer> source = new Random().nextBoolean()
                ? Flux.range(1,10).elementAt(5)
                : Flux.just(1,2,3,4).elementAt(5);

        source //
                .subscribeOn(Schedulers.parallel())
                .block();
    }
}