package com.esccase.blog;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.MediaType.TEXT_HTML;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
class JBlogApplicationTests {

    @Autowired
    private WebTestClient client;

    @Test
    void contextLoads() {
        client.get().uri("/").exchange() //
            .expectStatus().isOk() //
            .expectHeader().contentType(TEXT_HTML)
            .expectBody(String.class)
            .consumeWith(exchangeResult -> {
               assertThat(exchangeResult.getResponseBody()).contains("<a href=\"/add");
            });
    }

}
