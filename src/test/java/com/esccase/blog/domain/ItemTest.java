package com.esccase.blog.domain;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ItemTest {


    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void itemBasicsShouldWork(){
        Item oldItem = new Item("Apple AirPot", "애플 에어팟 이어폰", 24.55);

        assertThat( oldItem.getName()).isEqualTo("Apple AirPot");
        assertThat( oldItem.getDescription()).isEqualTo("애플 에어팟 이어폰");
        assertThat( oldItem.getPrice()).isEqualTo(24.55);

        Item newItem = new Item("Apple AirPot", "애플 에어팟 이어폰", 24.55);
        assertThat( oldItem).isEqualTo(newItem);

    }
}