package com.esccase.blog.service;

import com.esccase.blog.domain.Cart;
import com.esccase.blog.domain.CartItem;
import com.esccase.blog.domain.Item;
import com.esccase.blog.repository.CartRepository;
import com.esccase.blog.repository.ItemRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)  // JUnit5 테스트 핸들러 지정, 스프링에 특화된 테스트 기능 사용 가능
class CartServiceTest {
    private CartService cartService;

    @MockBean
    private ItemRepository itemRepository;
    @MockBean
    private CartRepository cartRepository;

    @BeforeEach
    void setUp() {
        // 테스트 데이터 정의
        Item item = new Item("item1", "TV tray", "Alf TV tray", 24.55);
        CartItem cartItem = new CartItem(item);
        Cart cart = new Cart("My Cart", Collections.singletonList(cartItem));

        // 협력자와의 상호작용 정의
        when( cartRepository.findById(anyString())).thenReturn(Mono.empty());
        when( itemRepository.findById(anyString())).thenReturn(Mono.just(item));
        when( cartRepository.save(any(Cart.class))).thenReturn(Mono.just(cart));

        // 가짜 협력자를 주입
        cartService = new CartService(itemRepository, cartRepository);
    }

    @AfterEach
    void tearDown() {
    }

    /**
     * top-level 방식의 테스트
     */
    @Test
    void addItemToEmptyCartShouldProduceOneCartItem() {
        cartService.addToCart("My Cart", "item1") //
            .as(StepVerifier::create) // 테스트 기능을 전담하는 리액터 타입 핸들러 생성
            .expectNextMatches(cart -> { //
                // 단언 1: 각 장바구니에 담긴 상품의 갯수를 추출하여, 한 가지 종류의 상품 1개만 들어 있음을 단언
                assertThat(cart.getCartItems()).extracting(CartItem::getQuantity) //
                    .containsExactlyInAnyOrder(1); //

                // 단언 2: 각 장바구니에 담긴 상품을 추출해서 한개인지를 검증하고 그 상품이 setUp() 에서 설정한 상품과 일치하는지 확인
                assertThat(cart.getCartItems()).extracting(CartItem::getItem) //
                    .containsExactly(new Item("item1", "TV tray", "Alf TV tray", 24.55)); //

                return true; // expectNextMatches 메소드의 반환 타입이 boolean 이므로 이 지점까지 통과했다면 성공으로 간주
            }) //
            .verifyComplete(); // 리액티브 스트림의 complete 시그널이 발생하고 리액터 플로우가 성공적으로 완료되었음을 검증
    }

    /**
     *
     */
    @Test
    void alternativeWayToTest() { // <1>
        StepVerifier.create( cartService.addToCart("My Cart", "item1")) //
            .expectNextMatches(cart -> { // <4>
                // 단언 1: 각 장바구니에 담긴 상품의 갯수를 추출하여, 한 가지 종류의 상품 1개만 들어 있음을 단언
                assertThat(cart.getCartItems()).extracting(CartItem::getQuantity)
                    .containsExactlyInAnyOrder(1);

                // 단언 2: 각 장바구니에 담긴 상품을 추출해서 한개인지를 검증하고 그 상품이 setUp() 에서 설정한 상품과 일치하는지 확인
                assertThat(cart.getCartItems()).extracting(CartItem::getItem)
                    .containsExactly(new Item("item1", "TV tray", "Alf TV tray", 24.55));

                return true; // expectNextMatches 메소드의 반환 타입이 boolean 이므로 이 지점까지 통과했다면 성공으로 간주
            }) //
            .verifyComplete(); // 리액티브 스트림의 complete 시그널이 발생하고 리액터 플로우가 성공적으로 완료되었음을 검증
    }


    @Test
    void removeToCart() {
    }

    @Test
    void removeOneFromCart() {
    }
}