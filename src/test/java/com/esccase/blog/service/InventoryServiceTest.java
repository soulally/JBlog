package com.esccase.blog.service;

import com.esccase.blog.domain.Cart;
import com.esccase.blog.domain.CartItem;
import com.esccase.blog.domain.Item;
import com.esccase.blog.repository.CartRepository;
import com.esccase.blog.repository.ItemRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.ReactiveFluentMongoOperations;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Mono;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)  // JUnit5 테스트 핸들러 지정, 스프링에 특화된 테스트 기능 사용 가능
class InventoryServiceTest {

    private InventoryService inventoryService;

    @MockBean
    private ItemRepository itemRepository;
    @MockBean
    private CartRepository cartRepository;
    @MockBean
    private ReactiveFluentMongoOperations fluentOperations;

    @BeforeEach
    void setUp() {
        // 테스트 데이터 정의
        Item item = new Item("item1", "TV tray", "Alf TV tray", 24.55);
        CartItem cartItem = new CartItem(item);
        Cart cart = new Cart("My Cart", Collections.singletonList(cartItem));

        // 협력자와의 상호작용 정의
        when( cartRepository.findById(anyString())).thenReturn(Mono.empty());
        when( itemRepository.findById(anyString())).thenReturn(Mono.just(item));
        when( cartRepository.save(any(Cart.class))).thenReturn(Mono.just(cart));

        // 가짜 협력자를 주입
        inventoryService = new InventoryService(itemRepository, cartRepository, fluentOperations);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void searchByExample() {

    }

    @Test
    void searchByFluentExample() {
    }
}