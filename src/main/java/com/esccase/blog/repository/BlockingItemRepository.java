package com.esccase.blog.repository;

import com.esccase.blog.domain.Item;
import org.springframework.data.repository.CrudRepository;

/**
 * 전통적인 블로킹 방식의 레파지토리
 */
@Deprecated
//@Component
public interface BlockingItemRepository extends CrudRepository<Item, String> {
}
