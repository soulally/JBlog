package com.esccase.blog.context;

import com.esccase.blog.domain.Item;
import com.esccase.blog.repository.BlockingItemRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


/**
 * 블로킹 방식의 기본 데이터 로딩 예제
 * 아래 클래스로 논블로킹 방식으로 로딩한다.
 * @see com.esccase.blog.context.TemplateDatabaseLoader
 */
@Deprecated
//@Component
public class RepositoryDatabaseLoader {

    @Bean
    CommandLineRunner initialize(BlockingItemRepository repository){
        return args -> {
            repository.save( new Item("Alf alarm clock", 19.99));
            repository.save( new Item("Smurf TV tray", 24.99));
        };
    }
}
