package com.esccase.blog.context;

import com.esccase.blog.domain.Item;
import com.esccase.blog.repository.BlockingItemRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Component;

@Component
public class TemplateDatabaseLoader {

    //@Bean
    CommandLineRunner initialize(MongoOperations mongo){
        return args -> {
            mongo.save( new Item("Alf alarm clock", "엘프 알람 시계",19.99));
            mongo.save( new Item("Smurf TV tray", "스머프 티비 트레이",24.80));
            mongo.save( new Item("Apple iMac", "애플 아이맥 27 인치 10세대",255.90));
            mongo.save( new Item("Apple Macbook", "애플 맥북 프로 M1X 칩셋",200.75));
            mongo.save( new Item("Apple Watch", "애플 워치 6세대 티타늄 에디션",60.66));
            mongo.save( new Item("Apple iPad", "애플 아이패드 프로 4세대 12.9 M1 칩셋",120.55));
        };
    }
}
