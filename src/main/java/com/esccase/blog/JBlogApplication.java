package com.esccase.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.thymeleaf.TemplateEngine;
import reactor.blockhound.BlockHound;
import reactor.core.publisher.Hooks;

@SpringBootApplication
public class JBlogApplication {

    public static void main(String[] args) {
        // TODO: 리엑터의 백트리이싱 활성화: 운영반영 시 조건에 따르 부분 적용 또는 삭제해야 함
        Hooks.onOperatorDebug();

        // 블록하운드 등록
        BlockHound.builder()
                .allowBlockingCallsInside( // 허용 리스트에 추가
                    TemplateEngine.class.getCanonicalName(), "process"
                )
                .install();

        SpringApplication.run(JBlogApplication.class, args);
    }
}
