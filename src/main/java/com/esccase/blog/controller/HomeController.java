package com.esccase.blog.controller;

import com.esccase.blog.domain.Cart;
import com.esccase.blog.domain.CartItem;
import com.esccase.blog.domain.Item;
import com.esccase.blog.domain.PageReq;
import com.esccase.blog.repository.CartRepository;
import com.esccase.blog.repository.ItemRepository;
import com.esccase.blog.service.CartService;
import com.esccase.blog.service.InventoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.result.view.Rendering;
import reactor.core.publisher.Mono;

@Slf4j
@Controller
@RequestMapping("/cart")
@RequiredArgsConstructor
public class HomeController {
    private final ItemRepository itemRepository;
    private final CartRepository cartRepository;
    private final CartService cartService;
    private final InventoryService inventoryService;

    /**
     * View/Attribute 를 포함하는 웹플럭스 컨테이너인 Mono<Rendering> 반환
     * @return
     */
    @GetMapping("/home")
    Mono<Rendering> home(){ //
       return Mono.just(
           Rendering.view("home") // View 로 렌더링에 사용할 템플릿 이름
               .modelAttribute("items", this.itemRepository.findAll().doOnNext(item -> log.info("item: {}", item)))
               // 데이터를 조회해서 없으면 새로운 데이터를 생성해서 반환하는 전형적인 리액터 사용법
               .modelAttribute("cart", this.cartRepository.findById("My Cart").defaultIfEmpty( new Cart("My Cart")))
               .build()
       );
    }

    @GetMapping("/search")
    Mono<Rendering> search(
              @RequestParam(required = false) String name
            , @RequestParam(required = false) String description
            , @RequestParam boolean useAnd) {
        log.info("name: {}, description: {}, useAnd: {}", name, description, useAnd);

        return Mono.just(
            Rendering.view("home")
                .modelAttribute("name", name)
                .modelAttribute("description", description)
                .modelAttribute("useAnd", useAnd)
                .modelAttribute("items", inventoryService.searchByExample(name,description,useAnd))
                .modelAttribute("cart", cartRepository.findById("My Cart").defaultIfEmpty( new Cart("My Cart")))
                .build()
        );
    }

    @PostMapping("/add/{id}")
    Mono<String> addToCart(@PathVariable String id){
        return this.cartService.addToCart("My Cart", id) //
            .thenReturn("redirect:/cart/home");
    }

    @DeleteMapping("/remove/{id}")
    Mono<String> removeToCart(@PathVariable String id){
        return this.cartService.removeToCart("My Cart", id) //
                .thenReturn("redirect:/cart/home");
    }

    @PostMapping("/create")
    Mono<String> createItem(@ModelAttribute Item newItem) {
        return this.itemRepository.save(newItem) //
                .thenReturn("redirect:/");
    }
}
