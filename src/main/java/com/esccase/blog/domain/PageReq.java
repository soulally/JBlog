package com.esccase.blog.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * 페이지 요청 처리 Dto
 */
@Data
@Builder
@AllArgsConstructor
public class PageReq {

    private int page;
    private int size;

    private String type;
    private String keyword;

    public PageReq(){
        this.page = 1;
        this.size = 10;
    }

    public Pageable getPageable(Sort sort){
        return org.springframework.data.domain.PageRequest.of(this.page - 1, this.size, sort);
    }
}
