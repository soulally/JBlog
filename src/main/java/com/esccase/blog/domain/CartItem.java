package com.esccase.blog.domain;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString

public class CartItem {
    private Item item;
    private int quantity;

    /**
     * private 생성자
     */
    private CartItem(){}

    public CartItem(Item item){
        this.item = item;
        this.quantity = 1;
    }

    public int increment(){
        return quantity++;
    }

    public int decrement(){
        quantity--;
        return quantity;
    }
}
