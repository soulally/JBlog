package com.esccase.blog.domain;

import lombok.*;
import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.List;

@Data
@ToString
public class Cart extends Base{
    @Id
    private String id;
    private List<CartItem> cartItems;

    /**
     * private 생성자
     */
    private Cart(){}

    public Cart(String id){
        this(id, new ArrayList<>());
    }

    public Cart(String id, List<CartItem> cartItems){
        this.id = id;
        this.cartItems = cartItems;
    }

    /**
     * 카트에서 아이템 제거
     */
    public boolean removeItem(CartItem cartItem){
        return this.cartItems.remove(cartItem);
    }
}
