package com.esccase.blog.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResDto<T> {
    HttpStatus status;
    int statusCode;
    T data;

    public ResDto(HttpStatus status, T data){
        this.status = status;
        this.statusCode = status.value();
        this.data = data;
    }
}
