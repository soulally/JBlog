package com.esccase.blog.domain;


import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 페이지 결과 처리 Dto
 */
@Data
@Slf4j
public class PageRes<DTO, EN> {
    private List<DTO> dtoList;
    private int totalPage;  // 총 페이지 번호
    private int page;       // 현재 페이지 번호
    private int size;       // 목록 사이즈
    private int start, end; // 시작 페이지 번호, 끝 페이지 번호
    private boolean prev, next;     // 이전, 다음
    private List<Integer> pageList; // 페이지 번호 목록


    /**
     * Page 객체로만 처리하는 생성자
     * @param result
     */
    public PageRes(Page<EN> result) {
        if (!result.getContent().isEmpty()) {
            dtoList = new ArrayList<>();
            for (EN content : result.getContent()) {
                // BaseEntity baseEntity = (BaseEntity) entity;
                dtoList.add((DTO) content);
            }
        }
        this.totalPage = result.getTotalPages();
        this.makePageList(result.getPageable());
    }

    /**
     * 펑션 파라미터를 전달하는 생성자
     * @param result
     * @param fn
     */
    public PageRes(Page<EN> result, Function<EN,DTO> fn) {
        this.dtoList = result.stream().map(fn).collect(Collectors.toList());
        this.totalPage = result.getTotalPages();
        this.makePageList(result.getPageable());
    }

    /**
     * 페이지 리스트 생성
     * @param pageable
     */
    private void makePageList(Pageable pageable){
        // 0 부터 시작하므로 1을 추가
        this.page = pageable.getPageNumber() + 1;
        this.size = pageable.getPageSize();

        // Temp end page
        int tempEnd = (int)(Math.ceil(this.page/10.)) * 10;
        this.start = tempEnd - 9;
        this.end = Math.min(tempEnd, this.totalPage);
        this.prev = 1 < start;
        this.next = tempEnd < this.totalPage;

        this.pageList = IntStream.rangeClosed(this.start, this.end).boxed().collect(Collectors.toList());
    }
}
