package com.esccase.blog.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public abstract class Base {
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

    // public abstract BaseEntity toEntity();
}
