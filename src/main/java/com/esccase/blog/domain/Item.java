package com.esccase.blog.domain;

import lombok.Data;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.util.StringUtils;

@Data
@ToString
public class Item extends Base{

    @Id
    private String id;
    private String name;
    private String description;
    private double price;

    /**
     * private 생성자
     */
    private Item(){}

    public Item(String name, double price){
        if (StringUtils.hasText(name)) this.name = name;
        this.price = price;
    }

    public Item(String name, String description, double price){
        if (StringUtils.hasText(name)) this.name = name;
        if (StringUtils.hasText(description)) this.description = description;
        this.price = price;
    }

    public Item(String id, String name, String description, double price){
        if (StringUtils.hasText(id)) this.id = id;
        if (StringUtils.hasText(name)) this.name = name;
        if (StringUtils.hasText(description)) this.description = description;
        this.price = price;
    }
}
