package com.esccase.blog.service;

import com.esccase.blog.domain.Cart;
import com.esccase.blog.domain.Item;
import com.esccase.blog.repository.CartRepository;
import com.esccase.blog.repository.ItemRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.mongodb.core.ReactiveFluentMongoOperations;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Slf4j
@Service
@RequiredArgsConstructor
public class InventoryService {

    private final ItemRepository itemRepository;
    private final CartRepository cartRepository;
    private final ReactiveFluentMongoOperations fluentOperations;

    public Mono<Cart> getCart(String cartId) {
        return this.cartRepository.findById(cartId);
    }

    public Flux<Item> getInventory() {
        return this.itemRepository.findAll();
    }

    Mono<Item> saveItem(Item newItem) {
        return this.itemRepository.save(newItem);
    }

    Mono<Void> deleteItem(String id) {
        return this.itemRepository.deleteById(id);
    }

    public Flux<Item> searchByExample(String name, String description, boolean useAnd){
        Item item  = new Item(name, description, 0.0);

        log.info("Search Item: {}", item);
        ExampleMatcher matcher = ( useAnd
            ? ExampleMatcher.matchingAll()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase()
                .withIgnorePaths("price")
            : ExampleMatcher.matchingAny()
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING)
                .withIgnoreCase()
                .withIgnorePaths("price")
        );

        Example<Item> probe = Example.of(item, matcher);
        return itemRepository.findAll(probe);
    }

    public Flux<Item> searchByFluentExample(String name, String description) {
        return fluentOperations.query(Item.class) //
            .matching(query(where("TV tray").is(name).and("Smurf").is(description))) //
            .all();
    }

}
