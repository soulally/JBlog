package com.esccase.blog.service;

import com.esccase.blog.domain.Cart;
import com.esccase.blog.domain.CartItem;
import com.esccase.blog.domain.Item;
import com.esccase.blog.repository.CartRepository;
import com.esccase.blog.repository.ItemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CartService {
    private final ItemRepository itemRepository;
    private final CartRepository cartRepository;


    public Mono<Cart> addToCart(String cartId, String itemId){
        return this.cartRepository.findById(cartId)
            .defaultIfEmpty( new Cart( cartId)) // 몽고 DB 에서 값이 없으면 defaultIfEmpty 를 통해 새로 생성해 반환한다.
            .flatMap(   // 장바구니 데이터를 가져온 후에 가장 먼저 해야 할 일은 장바구니에 담겨 있는 상품이 있는지를 확인하는 것이다.
                // 전통적인 기존 방식이라면 for-each 반복문으로 처리했겠지만, 자바의 스트림 API 덕분에 CartItem 을 순회하면서 새로 장바구니에 담은 것과 동일한 종류의 상품이 이미 있는지 확인할 수 있다.
                cart -> cart.getCartItems().stream()
                    // 동일한 종류의 상품 필터링
                    .filter( cartItem -> cartItem.getItem().getId().equals(itemId))
                    // findAny() 메소드는 Optional<CartItem> 을 반환한다.
                    .findAny()
                    .map( cartItem -> {
                        // 같은 상품이 있다면 해당 상품의 수량만 증가시키고 장바구니를 Mono 에 담아 반환한다.
                        if (10 > cartItem.getQuantity()) {
                            cartItem.increment();
                        }
                        return Mono.just(cart);
                    })
                    .orElseGet(()->
                        // 같은 상품이 없다면 몽고 DB 에서 해당 아이템을 조회하여 CartItem 에 담은 다음, CartItem 을 장바구니에 추가하여 장바구니를 반환한다.
                        this.itemRepository.findById(itemId)//
                            .map( item -> new CartItem(item)) //
                            .doOnNext( cartItem -> cart.getCartItems().add(cartItem))
                            .map( cartItem -> cart))
            )
            // 업데이트 된 장바구니 객체를 몽고 DB 에 저장한다.
            .flatMap( cart -> this.cartRepository.save(cart)
        );
    }


    public Mono<Cart> removeToCart(String cartId, String itemId){
        return this.cartRepository.findById(cartId)
            .flatMap(   // 장바구니 데이터를 가져온 후에 가장 먼저 해야 할 일은 장바구니에 담겨 있는 상품이 있는지를 확인하는 것이다.
                // 전통적인 기존 방식이라면 for-each 반복문으로 처리했겠지만, 자바의 스트림 API 덕분에 CartItem 을 순회하면서 새로 장바구니에 담은 것과 동일한 종류의 상품이 이미 있는지 확인할 수 있다.
                cart -> cart.getCartItems().stream()
                    // 동일한 종류의 상품 필터링
                    .filter( cartItem -> cartItem.getItem().getId().equals(itemId))
                    // findAny() 메소드는 Optional<CartItem> 을 반환한다.
                    .findAny()
                    .map( cartItem -> {
                        // 같은 상품이 있다면 해당 상품의 수량만 감소시키고 장바구니를 Mono 에 담아 반환한다.
                        if (0 >= cartItem.decrement()) {
                            cart.removeItem(cartItem);
                        }
                        return Mono.just(cart);
                    })
                    .orElseThrow( () -> {
                        throw new RuntimeException("오류: 해당 상품이 존재하지 않음");
                    })
            )
            // 업데이트 된 장바구니 객체를 몽고 DB 에 저장한다.
            .flatMap( cart -> this.cartRepository.save(cart)
        );
    }

    Mono<Cart> removeOneFromCart(String cartId, String itemId) {
        return this.cartRepository.findById(cartId)
                .defaultIfEmpty(new Cart(cartId))
                .flatMap(cart -> cart.getCartItems().stream()
                        .filter(cartItem -> cartItem.getItem().getId().equals(itemId))
                        .findAny()
                        .map(cartItem -> {
                            cartItem.decrement();
                            return Mono.just(cart);
                        }) //
                        .orElse(Mono.empty()))
                .map(cart -> new Cart(cart.getId(), cart.getCartItems().stream()
                        .filter(cartItem -> cartItem.getQuantity() > 0)
                        .collect(Collectors.toList())))
                .flatMap(cart -> this.cartRepository.save(cart));
    }
}
